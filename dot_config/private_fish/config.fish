

# Start X at login
if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        export MOZ_ENABLE_WAYLAND=1
        exec sway
    else if test -z "$DISPLAY" -a "$XDG_VTNR" = 2
        exec startx -- -keeptty
    end
end



# Environment Variables
export EDITOR=vim
export VISUAL=subl

# Git
set __fish_git_prompt_showcolorhints

set __fish_git_prompt_char_upstream_ahead " ↑"

set __fish_git_prompt_show_informative_status
set __fish_git_prompt_char_stateseparator " |"

set __fish_git_prompt_char_cleanstate " ✓"
set __fish_git_prompt_color_cleanstate green

set __fish_git_prompt_showdirtystate
set __fish_git_prompt_char_dirtystate " !"

set __fish_git_prompt_char_stagedstate " +"

set __fish_git_prompt_showuntrackedfiles
set __fish_git_prompt_char_untrackedfiles " ?"
